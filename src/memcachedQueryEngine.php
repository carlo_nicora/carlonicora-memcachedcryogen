<?php
/**
 * Copyright 2015 Carlo Nicora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @license Apache
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @package CarloNicora\cryogen\memcachedCryogen
 * @author Carlo Nicora
 */

namespace CarloNicora\cryogen\memcachedCryogen;

use CarloNicora\cryogen\queryEngine;

/**
 * The memcachedQueryEngine specialises the queryEngine to prepare the queries specifically for memcached
 */
class memcachedQueryEngine extends queryEngine{

    /**
     * Returns the type of field to be passed as type of parameters to memcached for the sql query preparation
     *
     * @param mixed $field
     * @return string
     */
    protected function getDiscriminantTypeCast($field=null){
        return(null);
    }

    /**
     * Generates the SQL statement needed for a count sql query
     *
     * @return string
     */
    public function generateReadCountStatement() {
        return(null);
    }

    /**
     * Geneates the SQL statement needed for a read sql query
     *
     * @return string
     */
    public function generateReadStatement() {
        return(null);
    }

    /**
     * Generates an array containing the parameters needed in WHERE or HAVING clauses
     *
     * @return array
     */
    public function generateReadParameters() {
        return(null);
    }

    /**
     * Generates the SQL statement needed for an UPDATE sql query
     *
     * @return string
     */
    public function generateUpdateStatement() {
        return(null);
    }

    /**
     * Generates an array containing the parameters needed in an UPDATE sql query
     *
     * @return array
     */
    public function generateUpdateParameters() {
        return(null);
    }

    /**
     * Generates the SQL statement needed for an INSERT sql query
     *
     * @return string
     */
    public function generateInsertStatement() {
        return(null);
    }

    /**
     * Generates an array containing the parameters needed in an INSERT sql query
     *
     * @return array
     */
    public function generateInsertParameters() {
        return(null);
    }

    /**
     * Generates the SQL statement needed for a DELETE sql query
     *
     * @return string
     */
    public function generateDeleteStatement() {
        return(null);
    }

    /**
     * Generates an array containing the parameters needed in aDELETE sql query
     *
     * @return array
     */
    public function generateDeleteParameters() {
        return(null);
    }
}