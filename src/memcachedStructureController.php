<?php
/**
 * Copyright 2015 Carlo Nicora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @license Apache
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @package CarloNicora\cryogen\mySqlCryogen;
 * @author Carlo Nicora
 */

namespace CarloNicora\cryogen\memcachedCryogen;

use CarloNicora\cryogen\structureController;
use CarloNicora\cryogen\metaTable;

/**
 * Implements the cryogen structure controller for memcached
 *
 */
class memcachedStructureController extends structureController{
    /**
     * @var memcachedConnectionController $connectionController
     */
    protected $connectionController;

    /**
     * @var memcachedCryogen cryogen
     */
    protected $cryogen;

    /**
     * Initialises the structure controller class
     *
     * @param memcachedConnectionController $connectionController
     * @param memcachedCryogen $cryogen
     */
    public function __construct(memcachedConnectionController $connectionController, memcachedCryogen $cryogen){
        $this->connectionController = $connectionController;
        $this->cryogen = $cryogen;
    }

    /**
     * Returns the structure of all the tables in the connected database
     *
     * @return array
     */
    public function readStructure(){
        return(null);
    }

    /**
     * Read the structure of a table from the database and returns the metaTable object
     *
     * @param $tableName
     * @return metaTable
     */
    public function readTableStructure($tableName){
        return(null);
    }

    /**
     * Creates a view based on the specified sql code
     *
     * @param $viewSql
     * @return bool
     */
    public function createView($viewSql){
        return(null);
    }

    /**
     * Creates a table on the database using the meta table passed as parameter
     *
     * @param metaTable $metaTable
     * @param bool $isFederated
     * @param string $federatedLink
     * @return bool
     */
    public function createTable(metaTable $metaTable, $isFederated=false, $federatedLink=null){
        return(null);
    }

    /**
     * Updates a table on the database using the meta table passed as parameter
     *
     * @param metaTable $metaTable
     * @return bool
     *
     * @todo Implement the method
     */
    public function updateTable(metaTable $metaTable){
        $returnValue = false;

        return($returnValue);
    }

    /**
     * Drops a table from the database using the meta table passed as parameter
     *
     * @param metaTable $metaTable
     * @return bool
     */
    public function dropTable(metaTable $metaTable){
        return(null);
    }

    /**
     * Truncates a table on the database using the meta table passed as parameter
     *
     * @param metaTable $metaTable
     * @return bool
     */
    public function truncateTable(metaTable $metaTable){
        return(null);
    }
}
?>